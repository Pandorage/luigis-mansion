﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Player
{
    public class Collisions : MonoBehaviour
    {
        public LayerMask groundLayer;
        public bool onGround;
        public float collisionRadius = 0.25f;
        public Vector2 bottomOffset;

        // Update is called once per frame
        void Update()
        {
            onGround = Physics2D.OverlapCircle((Vector2)transform.position + bottomOffset, collisionRadius, groundLayer);
        }
        
        void OnDrawGizmos()
        {
            Gizmos.color = Color.red;

            Gizmos.DrawWireSphere((Vector2)transform.position  + bottomOffset, collisionRadius);
        }
    }
}