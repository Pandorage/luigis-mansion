﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class ExplosiveBarrel : MonoBehaviour
{
    [SerializeField] private float explosionForce = 10;
    [SerializeField] private bool hasExploded = false;
    [SerializeField] private float magnitude;
    [SerializeField] private float magnitudeToExplode = 20;
    [SerializeField] private float radiusExplosion = 5f;

    private Rigidbody2D _rb;
    private Collider2D _collider;

    private void Start()
    {
        // Get the Rigidbody2D and Collider2D
        _rb = GetComponent<Rigidbody2D>();
        _collider = GetComponent<Collider2D>();
    }

    void Update()
    {
        // Get the magnitude
        magnitude = _rb.velocity.magnitude;
        
        if (magnitude >= magnitudeToExplode)
        {
            // improves barrel explosion
            _collider.isTrigger = true;
        }
        else
        {
            _collider.isTrigger = false;
        }
    }

    void OnTriggerStay2D(Collider2D collision)
    {
        // If the barrel is thrown by a strong enough force
        if (magnitude >= magnitudeToExplode && !hasExploded)
        {
            // EXPLOSION
            Exploded();
            hasExploded = true;
        }
    }

    private void Exploded()
    {
        // Get all the colliders in the radius explosion
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, radiusExplosion);
        foreach (Collider2D nearbyObject in colliders)
        {
            Rigidbody2D rb = nearbyObject.GetComponent<Rigidbody2D>();
            if (rb != null)
            {
                // Add force
                rb.AddForce((rb.transform.position - transform.position).normalized * explosionForce, ForceMode2D.Impulse);
            }
        }
        // Destroy the barrel
        Destroy(gameObject);
    }
}
