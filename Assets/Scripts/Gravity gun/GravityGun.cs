﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GravityGun : MonoBehaviour
{
    [SerializeField] private float range = 10;
    [SerializeField] private Transform gunPoint;
    [SerializeField] private LayerMask obstacleLayer;
    [SerializeField] private float launchForce = 5;
    [SerializeField] private Slider launchBar;
    [SerializeField] private float fillSpeed = 1;
    [SerializeField] private Rigidbody2D rbPlayer;
    private Camera _cam;
    private GameObject _target;
    private float _launchRatio;

    private void Start()
    {
        _cam = Camera.main;
    }

    void Update()
    {
        Vector2 lookDir = _cam.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        float angle = Mathf.Atan2(lookDir.y, lookDir.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0, 0, angle);
        Debug.DrawRay(gunPoint.position, gunPoint.right * range);

        if (Input.GetButtonUp("Fire1"))
        {
            if (_target == null)
            {
                var hit = Physics2D.Raycast(gunPoint.position, gunPoint.right, range,obstacleLayer);    
                if (hit.collider != null && hit.collider.gameObject.CompareTag("Target"))
                {
                    _target = hit.collider.gameObject;
                    _target.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                    _target.GetComponent<Rigidbody2D>().simulated = false;
                    _target.GetComponent<Collider2D>().enabled = false;
                    _target.transform.parent = gunPoint;
                    _target.transform.localPosition = Vector3.zero;
                }
            }
            else if(!Physics2D.OverlapCircle(gunPoint.position, 0.25f, obstacleLayer))
            {
                _target.transform.parent = null;
                _target.transform.rotation = Quaternion.Euler(0,0,0);
                _target.GetComponent<Rigidbody2D>().simulated = true;
                _target.GetComponent<Rigidbody2D>().velocity = rbPlayer.velocity;
                _target.GetComponent<Rigidbody2D>().AddForce(gunPoint.right * _launchRatio, ForceMode2D.Impulse);
                _target.GetComponent<Collider2D>().enabled = true;
                _target = null;
            }
            launchBar.gameObject.SetActive(false);
            _launchRatio = 0;
        }
        if (Input.GetButton("Fire1"))
        {
            if (_target != null)
            {
                if (!launchBar.IsActive())
                {
                    launchBar.gameObject.SetActive(true);
                }
                if( _launchRatio < launchForce)
                {
                    _launchRatio += fillSpeed * Time.deltaTime;
                    if (_launchRatio > launchForce)
                    {
                        _launchRatio = launchForce;
                    }
                    launchBar.value = _launchRatio / launchForce;
                }
            }
        }
    }
}