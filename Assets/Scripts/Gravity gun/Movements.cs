﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Player
{
    public class Movements : MonoBehaviour
    {
        public float speed;
        public float jumpForce;

        private Rigidbody2D _rb;
        private Collisions _coll;
        private SpriteRenderer _sprite;
        void Start()
        {
            _rb = GetComponent<Rigidbody2D>();
            _coll = GetComponent<Collisions>();
            _sprite = GetComponent<SpriteRenderer>();
        }
        
        void Update()
        {
            float x = Input.GetAxis("Horizontal");
            float y = Input.GetAxis("Vertical");
            Vector2 dir = new Vector2(x, y);

            if (x > 0)
            {
                _sprite.flipX = false;
            }
            else if (x < 0)
            {
                _sprite.flipX = true;
            }

            Walk(dir);
            
            if(Input.GetButtonDown("Jump") && _coll.onGround)
            {
                Jump();
            }
        }

        private void Walk(Vector2 dir)
        {
            _rb.velocity = new Vector2(dir.x * speed, _rb.velocity.y);
        }

        private void Jump()
        {
            _rb.velocity = new Vector2(_rb.velocity.x, 0);
            _rb.velocity += Vector2.up * jumpForce;
        }
    }
}