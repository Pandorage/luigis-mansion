﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Torchlight : MonoBehaviour
{
    public GameObject torchlight;
    [SerializeField] private float timerTorchlight = 5f;
    [SerializeField] private float durationLight = 0.2f;
    private float _nextTorchlightUse;
    private float _torchLightOff;

    // Update is called once per frame
    void Update()
    {
        UseTorchlight();
    }

    void UseTorchlight()
    {
        // Switch on the torchlight
        if (Input.GetButtonDown("Fire2") && Time.time >= _nextTorchlightUse)
        {
            torchlight.SetActive(true);
            _nextTorchlightUse = Time.time + timerTorchlight;
            _torchLightOff = Time.time + durationLight;
        }
        
        if (Time.time >= _torchLightOff)
        {
            torchlight.SetActive(false);
        }
    }
}