﻿using UnityEngine;

namespace Luigis_mansion
{
    public class Vacuum : MonoBehaviour
    {
        public Camera cam;
        private Vector2 _mousePos;
        public Animator animator;
    
        void Update()
        {
            _mousePos = cam.ScreenToWorldPoint(Input.mousePosition);
        }
    
        void FixedUpdate()
        {
            Vector2 lookDir = _mousePos - (Vector2)transform.position;
            float angle = Mathf.Atan2(lookDir.y, lookDir.x) * Mathf.Rad2Deg - 90f;
            transform.rotation = Quaternion.Euler(0, 0, angle);
            lookDir.Normalize();
            
            animator.SetFloat("Horizontal", lookDir.x);
            animator.SetFloat("Vertical", lookDir.y);
        }
    }
}
