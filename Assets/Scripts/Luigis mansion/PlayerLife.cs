﻿using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Luigis_mansion
{
    public class PlayerLife : MonoBehaviour
    {
        [SerializeField] private int maxLife = 10;
        [SerializeField] private Slider lifeBar;
        [SerializeField] private float dodgeDuration = 1;
        public SpriteRenderer sr;
        private int _life;
        private bool _dodge;

        private void Start()
        {
            _life = maxLife;
        }

        private void UpdateLifeBar()
        {
            lifeBar.value = (float)_life/maxLife;
        }

        public void Damage(int dmg)
        {
            if (!_dodge)
            {
                _dodge = true;
                _life -= dmg;
                if (_life <= 0)
                {
                    _life = 0;
                    UpdateLifeBar();
                    FindObjectOfType<GameManager>().EndGame();
                    //gameObject.SetActive(false);
                }
                UpdateLifeBar();
                sr.DOColor(new Color(1, 0, 0, 0.5f), dodgeDuration / 6).SetLoops(6, LoopType.Yoyo);
                Invoke(nameof(EndDodge), dodgeDuration);
            }
        }

        public void EndDodge()
        {
            _dodge = false;
        }
    }
}
