﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Luigis_mansion;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;

public class CaptureScript : MonoBehaviour
{
    public float speed;
    public GameObject torchlight;
    public float lightCooldown = 2;
    public ParticleSystem vacuum;
    [Space]

    [Header("Ghost")]
    public GhostScript currentGhost;
    public List<GhostScript> lightedGhosts;

    [Space]
    [Header("Booleans")]
    public bool capturing;

    private Vector3 axis;
    
    public RandomRotation randomRot;
    private float _nextLight;
    
    // UI
    private int countGhostCaptured;
    public Text nbGhostText;
    public Animator anim;

    void Update()
    {
        if (Input.GetButtonDown("Fire2") && !capturing && Time.time >= _nextLight)
        {
            _nextLight = Time.time + lightCooldown;
            foreach (var ghost in lightedGhosts)
            {
                ghost.Stun();
            }
            torchlight.GetComponent<SpriteRenderer>().DOFade(1, 0.1f).SetLoops(2, LoopType.Yoyo).SetEase(Ease.OutQuint);
        }
        if (Input.GetButtonDown("Fire1") && !capturing)
        {
            foreach (var ghost in lightedGhosts)
            {
                if (ghost.stunned)
                {
                    currentGhost = ghost;
                    Vector2 ghostDir = currentGhost.transform.position - randomRot.transform.position;
                    float rotateAngle = Mathf.Atan2(ghostDir.y, ghostDir.x) * Mathf.Rad2Deg - 90f;
                    randomRot.transform.rotation = Quaternion.Euler(0, 0, rotateAngle);
                    currentGhost.ActivateEscapeRig();
                    GetComponent<PlayerMovements>().enabled = false;
                    FindObjectOfType<Vacuum>().enabled = false;
                    CaptureState(true);
                    break;
                }
            }
        }

        if (!capturing)
            return;

        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");
        axis = new Vector2(x, y).normalized;
        float angle = Vector2.Angle(randomRot.transform.up, axis);
        GetComponent<Rigidbody2D>().MovePosition(transform.position + randomRot.transform.up* speed * Time.deltaTime);
        Vector2 dir = (currentGhost.transform.position - transform.position).normalized;
        anim.SetFloat("Horizontal", dir.x);
        anim.SetFloat("Vertical", dir.y);
        anim.SetFloat("Speed", 1);
        if(angle > 90)
        {
            GetComponent<Rigidbody2D>().MovePosition(transform.position+ axis * (speed/2) * Time.deltaTime);

            if (currentGhost.energy > 0)
                currentGhost.Damage(angle);
            else
                Capture();
        }
    }
    
    private void Capture()
    {
        capturing = false;
        GetComponent<PlayerMovements>().enabled = true;
        FindObjectOfType<Vacuum>().enabled = true;
        currentGhost.Capture();
        CaptureState(false);
        countGhostCaptured++;
        nbGhostText.text = countGhostCaptured.ToString();
        currentGhost = null;
    }

    public void FailCapture()
    {
        capturing = false;
        CaptureState(false);
        GetComponent<PlayerMovements>().enabled = true;
        FindObjectOfType<Vacuum>().enabled = true;
        currentGhost = null;
    }
    
    public void CaptureState(bool state)
    {
        capturing = state;
        torchlight.SetActive(!state);
        randomRot.enabled = state;
        if (state)
        {
            vacuum.Play();
        }
        else
        {
            vacuum.Stop();
        }
    }
}
