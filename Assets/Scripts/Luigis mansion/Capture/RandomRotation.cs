﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomRotation : MonoBehaviour
{
    private bool _right;

    public float rotateSpeed = 10;
    public AnimationCurve lerpEase = default;
    public float zRot;
    
    private void OnEnable()
    {
        StartCoroutine(RotateTo());
        StartCoroutine(ChooseDir());
    }
    
    IEnumerator RotateTo()
    {
        zRot += Random.Range(15, 45) * (_right ? 1 : -1);
        float distance = Mathf.Abs(Mathf.DeltaAngle(transform.localEulerAngles.z, zRot));

        Quaternion startRot = transform.rotation;
        Quaternion endRot = Quaternion.Euler(0, 0, zRot);

        float animateTime = 0;
        float animationLength = distance / rotateSpeed;

        while (animateTime < animationLength)
        {
            animateTime += Time.deltaTime;
            transform.rotation = Quaternion.Slerp(startRot, endRot, lerpEase.Evaluate(animateTime / (animationLength)));
            yield return null;
        }
        
        StartCoroutine(RotateTo());
    }
    
    IEnumerator ChooseDir()
    {
        _right = Random.value > 0.5f;
        yield return new WaitForSeconds(Random.Range(0.2f, 1));
        StartCoroutine(ChooseDir());
    }
    
    private void OnDisable()
    {
        StopCoroutine(nameof(RotateTo));
        StopCoroutine(nameof(ChooseDir));
        StopAllCoroutines();
    }
    
    private void OnDestroy()
    {
        if(FindObjectOfType<CaptureScript>() != null)
            transform.rotation = FindObjectOfType<CaptureScript>().transform.rotation;
    }
}
