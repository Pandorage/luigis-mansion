﻿using System;
using System.Collections;
using DG.Tweening;
using Luigis_mansion;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

public class GhostScript : MonoBehaviour
{
    private CaptureScript _capture;

    [Space]
    [Header("Public")]
    public bool escaping;
    public bool stunned;
    public float stunDuration;
    public float energy = 100;
    public TMP_Text energyText;
    public SpriteRenderer sr;
    public Animator animator;
    
    private GhostMove _move;
    private float _endStun;
    private bool _reseting;
    private Collider2D _collider;
    private Vector2 _startPos;
    
    private void Start()
    {
        _collider = GetComponent<Collider2D>();
        _capture = FindObjectOfType<CaptureScript>();
        _move = GetComponent<GhostMove>();
        _startPos = transform.position;
    }

    private void Update()
    {
        transform.rotation = Quaternion.Euler(0,0,0);

        if (escaping)
        {
            energyText.text = ((int)energy).ToString();
            Vector2 dir = -(_capture.transform.position - transform.position).normalized;
            animator.SetFloat("Horizontal", dir.x);
            animator.SetFloat("Vertical", dir.y);
        }

        if (!escaping && stunned && Time.time >= _endStun)
        {
            stunned = false;
            _move.enabled = true;
            sr.DOFade(0.5f, 0.1f);
        }
    }

    public void Stun()
    {
        _move.enabled = false;
        sr.DOFade(1, 0.1f);
        stunned = true;
        _endStun = Time.time + stunDuration;
    }
   
    public void ActivateEscapeRig()
    {
        transform.parent = _capture.randomRot.transform;
        transform.DOLocalMove(new Vector2(0, 2.5f), 0.5f);
        energyText.gameObject.SetActive(true);
        escaping = true;
    }
    
    public void Capture()
    {
        StartCoroutine(DestroyGhost());
    }
    
    IEnumerator DestroyGhost()
    {
        _collider.enabled = false;
        transform.DOLocalMove(Vector3.zero, 0.5f);
        transform.DOScale(0, 0.5f);
        sr.DOFade(0, 0.5f);
        yield return new WaitForSeconds(0.5f);
        Destroy(gameObject);
    }
    public void Damage(float angle)
    {
        float damage = Remap(angle, 90, 180, .2f, .1f);
        energy = Mathf.Max(0,energy - (.3f - damage));
    }

    public void Reset()
    {
        DOTween.Kill(transform);
        transform.parent = null;
        _collider.enabled = false;
        _capture.FailCapture();
        stunned = false;
        escaping = false;
        energyText.gameObject.SetActive(false);
        sr.DOFade(0, 0.5f);
        transform.DOMove(_startPos, 2.5f);
        StartCoroutine(Respawn());
    }

    private IEnumerator Respawn()
    {
        yield return new WaitForSeconds(2);
        sr.DOFade(0.5f, 0.5f);
        yield return new WaitForSeconds(0.5f);
        _collider.enabled = true;
        _move.enabled = true;
        _move.Reset();
    }

    public float Remap(float value, float from1, float to1, float from2, float to2)
    {
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        if ((escaping || stunned) && other.gameObject.CompareTag("Wall"))
        {
            Reset();
        }
    }
}
