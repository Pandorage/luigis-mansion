﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostTrigger : MonoBehaviour
{
    private CaptureScript _capture;

    private void Start()
    {
        _capture = FindObjectOfType<CaptureScript>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Ghost"))
        {
            if (!_capture.lightedGhosts.Contains(other.GetComponent<GhostScript>()))
            {
                _capture.lightedGhosts.Add(other.GetComponent<GhostScript>());
            } 
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Ghost"))
        {
            _capture.lightedGhosts.Remove(other.GetComponent<GhostScript>());
        }
    }
}
