﻿using System;
using System.Collections;
using System.Collections.Generic;
using Luigis_mansion;
using UnityEngine;
using Random = UnityEngine.Random;

public class GhostMove : MonoBehaviour
{
    public float moveRange = 3;
    public float speed = 3;
    public float detectionRange = 4;
    public float followRange = 8;
    public Animator animator;
    private Vector2 _movePos;
    private Vector2 _startPos;
    private bool _attacking;
    private Transform _player;
    [SerializeField] private Pattern pattern;
    
    private enum Pattern
    {
        Random,
        Follow,
        Flee,
    }
    private void Start()
    {
        _startPos = transform.position;
        _player = FindObjectOfType<CaptureScript>().transform;
        StartCoroutine(ChangePos());
    }

    void Update()
    {
        if (Vector2.Distance(transform.position, _movePos) > 0.01)
        {
            transform.position = Vector3.MoveTowards(transform.position, _movePos, Time.deltaTime*speed);
            Vector2 vec = (_movePos - (Vector2)transform.position).normalized;
            
            // Animation
            animator.SetFloat("Horizontal", vec.x);
            animator.SetFloat("Vertical", vec.y);
        }

        if (Vector2.Distance(_player.position, transform.position) <= detectionRange && !_attacking)
        {
            _attacking = true;
            StopAllCoroutines();
            switch (pattern)
            {
                case Pattern.Random:
                    StartCoroutine(RandomMove());
                    break;
                case Pattern.Follow:
                    StartCoroutine(Follow());
                    break;
                case Pattern.Flee:
                    StartCoroutine(Flee());
                    break;
            }
        }

        if (Vector2.Distance(_player.position, transform.position) > followRange && _attacking)
        {
            Reset();
        }
    }
    
    private IEnumerator ChangePos()
    {
        _movePos = _startPos + Random.insideUnitCircle*moveRange;
        yield return new WaitForSeconds(Random.Range(2f, 4f));
        StartCoroutine(ChangePos());
    }

    private IEnumerator RandomMove()
    {
        _movePos = (Vector2) _player.position + Random.insideUnitCircle.normalized * 2;
        yield return new WaitForSeconds(Random.Range(0.5f,1f));
        StartCoroutine(RandomMove());
    }

    private IEnumerator Flee()
    {
        _movePos = transform.position + (transform.position - _player.position).normalized * 3;
        yield return new WaitForSeconds(0.5f);
        StartCoroutine(Flee());
    }

    private IEnumerator Follow()
    {
        _movePos = _player.position - (transform.position - _player.position).normalized * 2;
        yield return new WaitForSeconds(1);
        StartCoroutine(Follow());
    }

    public void Reset()
    {
        StopAllCoroutines();
        _attacking = false;
        _movePos = _startPos;
        StartCoroutine(ChangePos());
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.GetComponent<PlayerLife>() != null)
        {
            other.gameObject.GetComponent<PlayerLife>().Damage(1);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, detectionRange);
    }
}
