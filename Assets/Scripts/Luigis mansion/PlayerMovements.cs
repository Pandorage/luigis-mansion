﻿using UnityEngine;

namespace Luigis_mansion
{
    public class PlayerMovements : MonoBehaviour
    {

        public float moveSpeed = 5.0f;
        public Rigidbody2D rb;
        private Vector2 _movement;
        public Animator animator;

        // Update is called once per frame
        void Update()
        {
            _movement.x = Input.GetAxisRaw("Horizontal");
            _movement.y = Input.GetAxisRaw("Vertical");
            // Adapt the diagonal movement
            _movement.Normalize();
            animator.SetFloat("Speed", _movement.sqrMagnitude);
        }

        void FixedUpdate()
        {
            rb.MovePosition(rb.position + _movement * (moveSpeed * Time.fixedDeltaTime));
        }
    }
}
