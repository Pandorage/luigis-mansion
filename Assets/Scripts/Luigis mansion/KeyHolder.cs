﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KeyHolder : MonoBehaviour
{
    public Text textNbSliverKey;
    public Text textNbGoldenKey;
    
    // Key list
    private List<Key.KeyType> keyList;
   [SerializeField] private int countNbSilverKey = 0;
   [SerializeField] private int countNbGoldenKey = 0;

    private void Awake()
    {
        // Init
        keyList = new List<Key.KeyType>();
    }

    public void AddKey(Key.KeyType keyType)
    {
        Debug.Log("Added key : " + keyType);
        // Add key
        keyList.Add(keyType);
        // Update UI
        if (keyType.Equals(Key.KeyType.GoldenKey))
        {
            countNbSilverKey++;
            textNbGoldenKey.text = countNbSilverKey.ToString();
        }
        
        if (keyType.Equals(Key.KeyType.SilverKey))
        {
            countNbGoldenKey++;
            textNbSliverKey.text = countNbGoldenKey.ToString();
        }
    }

    public void RemoveKey(Key.KeyType keyType)
    {
        // Remove key
        keyList.Remove(keyType);
        
        // Update UI
        if (keyType.Equals(Key.KeyType.GoldenKey))
        {
            countNbSilverKey--;
            textNbGoldenKey.text = countNbSilverKey.ToString();
        }
        
        if (keyType.Equals(Key.KeyType.SilverKey))
        {
            countNbGoldenKey--;
            textNbSliverKey.text = countNbGoldenKey.ToString();
        }
    }

    public bool ContainsKey(Key.KeyType keyType)
    {
        // Check key
        return keyList.Contains(keyType);
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        // collision with a key
        Key key = collider.GetComponent<Key>();
        
        if (key != null)
        {
            // Get a key
            AddKey(key.GetKeyType());
            
            // Delete key from the game
            Destroy(key.gameObject);
        }

        // collision with a door
        KeyDoor keyDoor = collider.GetComponent<KeyDoor>();
        if (keyDoor != null)
        {
            // Check the type of key for the door
            if (ContainsKey(keyDoor.GetKeyType()))
            {
                // Remove the key from list
                RemoveKey(keyDoor.GetKeyType());
                
                // Holding key to open this door
                keyDoor.OpenDoor();
            }
        }
    }
}
