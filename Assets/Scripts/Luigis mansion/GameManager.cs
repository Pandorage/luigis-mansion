﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private bool gameHasEnded = false;
    private float restartDelay = 1f;
    public void EndGame()
    {
        if (!gameHasEnded)
        {
            gameHasEnded = true;
            Invoke("Restart", restartDelay);
        }
    }

    void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    void Menu()
    {
        SceneManager.LoadScene(0);
    }

    private void Update()
    {
        if (FindObjectsOfType<GhostScript>().Length == 0)
        {
            Invoke("Menu", restartDelay);
        }
    }
}
